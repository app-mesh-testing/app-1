FROM golang:1.14.0

WORKDIR /app

RUN go get -u github.com/gin-gonic/gin
RUN mkdir -p /app

COPY ./src .

RUN go build -o main ./app.go \
    && chmod +x ./main

EXPOSE 8080

ENTRYPOINT [ "./main" ]
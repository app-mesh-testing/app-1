package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	r := gin.Default()

	r.Use(RequestLoggerMiddleware())

	getProductsUrl := os.Getenv("GET_PRODUCTS_URL")

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.GET("/test", func(c *gin.Context) {
		if getProductsUrl == "" {
			log.Panicln("GET_PRODUCTS_URL environment variable not set")
		}
		
		log.Println("Calling " + getProductsUrl + " to get products")

		resp, err := http.Get(getProductsUrl)
		if err != nil {
			log.Panicln("Failed getting products: " + err.Error())
		}

		var products map[string]interface{}
		_ = json.NewDecoder(resp.Body).Decode(&products)

		defer resp.Body.Close()

		log.Println("Products retrieved successfully: " + fmt.Sprint(products))

		c.JSON(200, gin.H{
			"message": "success",
			"products": products["data"],
		})
	})

	r.Run(":8080") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

func RequestLoggerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		var buf bytes.Buffer
		tee := io.TeeReader(c.Request.Body, &buf)
		body, _ := ioutil.ReadAll(tee)
		c.Request.Body = ioutil.NopCloser(&buf)
		log.Println("Body: " + string(body))
		log.Println("Header: " + fmt.Sprint(c.Request.Header))
		c.Next()
	}
}